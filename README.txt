
Description
-----------
This module provides a central function for adding Freewall jQuery plugin
elements. For more information about Freewall, visit the official project:
http://vnjs.net/www/project/freewall/


Installation
------------
1) Install the Libraries module (http://drupal.org/project/libraries) on your Drupal
   site if you have not already.

2) Install the jQuery Update module (http://drupal.org/project/jquery_update) on your Drupal
   site if you have not already. Use version 1.7 of jQuery.

3) Download Frewall plugin (https://github.com/kombai/freewall/archive/master.zip) and place
   in libraries folder (this will usually be "sites/all/libraries/").

4) Place this module directory in your modules folder (this will usually be
   "sites/all/modules/").

5) Enable the module within your Drupal site at Administer -> Site Building ->
   Modules (admin/build/modules).

Usage
-----
The Freewall module is most commonly used with the Views module to turn
listings of images or other content into a responsive grid.

1) Install the Views module (http://drupal.org/project/views) on your Drupal
   site if you have not already.

2) Add a new view at Administration -> Structure -> Views (admin/structure/views).

3) Change the "Display format" of the view to "Freewall". Click the
   "Continue & Edit" button to configure the rest of the View.

4) Click on the "Settings" link next to the Freewall Format to configure the
   options for the grid.

5) Add the items you would like to include in the grid under the "Fields"
   section, and build out the rest of the view as you would normally. Note that
   the preview of the grid within Views probably will not appear correctly
   because the necessary JavaScript and CSS is not loaded in the Views
   interface. Save your view and visit a page URL containing the view to see
   how it appears.

API Usage
---------
The freewall_add function allows you to not only add the required Freewall
JavaScript, and apply the Freewall behavior to the elements on the page. The
arguments are as follows:

  freewall_add($selector, $options);

The $selector is the jQuery selector of the element which will become the grid. Multiple
grids may have the same selector and all their options will be shared.

The $options are the configuration options that are sent during the creation
of the freewall grid element (optional). The configuration options can be found at:
http://vnjs.net/www/project/freewall/#options

An alternative to using freewall_add() is passing a list of items that will be
in your grid into theme('freewall'). This can be useful to not only add
the necessary JavaScript and CSS to the page but also to print out the HTML
list.

  print theme('freewall', array('items' => $items, 'options' => $options, 'identifier' => $identifier));


Example
-------
The following would add a grid to the page:

  <div class="freewall">
    <div class="brick"><img src="http://vnjs.net/www/project/freewall/example/i/photo/1.jpg" alt="" /></div>
    <div class="brick"><img src="http://vnjs.net/www/project/freewall/example/i/photo/2.jpg" alt="" /></div>
    <div class="brick"><img src="http://vnjs.net/www/project/freewall/example/i/photo/3.jpg" alt="" /></div>
    <div class="brick"><img src="http://vnjs.net/www/project/freewall/example/i/photo/4.jpg" alt="" /></div>
    <div class="brick"><img src="http://vnjs.net/www/project/freewall/example/i/photo/5.jpg" alt="" /></div>
    <div class="brick"><img src="http://vnjs.net/www/project/freewall/example/i/photo/6.jpg" alt="" /></div>
    <div class="brick"><img src="http://vnjs.net/www/project/freewall/example/i/photo/7.jpg" alt="" /></div>
    <div class="brick"><img src="http://vnjs.net/www/project/freewall/example/i/photo/8.jpg" alt="" /></div>
    <div class="brick"><img src="http://vnjs.net/www/project/freewall/example/i/photo/9.jpg" alt="" /></div>
    <div class="brick"><img src="http://vnjs.net/www/project/freewall/example/i/photo/10.jpg" alt="" /></div>
  </div>
  <?php
    freewall_add('.freewall', array('animate' => true));
  ?>


Authors
-------
David Lazarte (http://www.davidlazarte.com/)