<?php

/**
 * @file freewall.tpl.php
 * Template to display a list as a responsive grid layouts.
 */

?>
<div id="<?php print $identifier; ?>">
<?php foreach ($items as $id => $item): ?>
  <div <?php print $item['attributes']; ?>><?php print $item['data']; ?></div>
<?php endforeach; ?>
</div>