<?php
/**
 * @file
 * freewall_example_views.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function freewall_example_views_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function freewall_example_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function freewall_example_views_node_info() {
  $items = array(
    'brick' => array(
      'name' => t('Brick'),
      'base' => 'node_content',
      'description' => t('A brick in de wall.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
