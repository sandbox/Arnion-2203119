<?php
/**
 * @file
 * freewall_example_views.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function freewall_example_views_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_brick_body'
  $field_bases['field_brick_body'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_brick_body',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_with_summary',
  );

  // Exported field_base: 'field_brick_image'
  $field_bases['field_brick_image'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_brick_image',
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_brick_size'
  $field_bases['field_brick_size'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_brick_size',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'brick-1-1' => 'brick-1-1',
        'brick-1-2' => 'brick-1-2',
        'brick-1-3' => 'brick-1-3',
        'brick-2-1' => 'brick-2-1',
        'brick-2-2' => 'brick-2-2',
        'brick-2-3' => 'brick-2-3',
        'brick-3-1' => 'brick-3-1',
        'brick-3-2' => 'brick-3-2',
        'brick-3-3' => 'brick-3-3',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_brick_style'
  $field_bases['field_brick_style'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_brick_style',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'red' => 'red',
        'orange' => 'orange',
        'yellow' => 'yellow',
        'green' => 'green',
        'blue' => 'blue',
        'purple' => 'purple',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
