<?php
/**
 * @file
 * freewall_example_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function freewall_example_views_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'freewall';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Freewall';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Freewall';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'freewall';
  $handler->display->display_options['style_options']['row_class'] = 'brick [field_brick_style] [field_brick_size]';
  $handler->display->display_options['style_options']['row_style'] = '';
  $handler->display->display_options['style_options']['delay'] = '0';
  $handler->display->display_options['style_options']['cellW'] = '';
  $handler->display->display_options['style_options']['cellH'] = '';
  $handler->display->display_options['style_options']['gutterX'] = '';
  $handler->display->display_options['style_options']['gutterY'] = '';
  $handler->display->display_options['style_options']['fixSize'] = '';
  $handler->display->display_options['style_options']['draggable'] = 0;
  $handler->display->display_options['style_options']['animate'] = 1;
  $handler->display->display_options['style_options']['rightToLeft'] = 0;
  $handler->display->display_options['style_options']['bottomToTop'] = 0;
  $handler->display->display_options['style_options']['onGapFound'] = '';
  $handler->display->display_options['style_options']['onComplete'] = '';
  $handler->display->display_options['style_options']['onResize'] = 'this.fitWidth();';
  $handler->display->display_options['style_options']['onBlockReady'] = '';
  $handler->display->display_options['style_options']['onBlockActive'] = '';
  $handler->display->display_options['style_options']['onBlockFinish'] = '';
  $handler->display->display_options['style_options']['before'] = '';
  $handler->display->display_options['style_options']['after'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Content: Image (field_brick_image:fid) */
  $handler->display->display_options['relationships']['field_brick_image_fid']['id'] = 'field_brick_image_fid';
  $handler->display->display_options['relationships']['field_brick_image_fid']['table'] = 'field_data_field_brick_image';
  $handler->display->display_options['relationships']['field_brick_image_fid']['field'] = 'field_brick_image_fid';
  $handler->display->display_options['relationships']['field_brick_image_fid']['label'] = 'image';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_type'] = 'div';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  /* Field: Content: Style */
  $handler->display->display_options['fields']['field_brick_style']['id'] = 'field_brick_style';
  $handler->display->display_options['fields']['field_brick_style']['table'] = 'field_data_field_brick_style';
  $handler->display->display_options['fields']['field_brick_style']['field'] = 'field_brick_style';
  $handler->display->display_options['fields']['field_brick_style']['label'] = '';
  $handler->display->display_options['fields']['field_brick_style']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_brick_style']['element_label_colon'] = FALSE;
  /* Field: Content: Size */
  $handler->display->display_options['fields']['field_brick_size']['id'] = 'field_brick_size';
  $handler->display->display_options['fields']['field_brick_size']['table'] = 'field_data_field_brick_size';
  $handler->display->display_options['fields']['field_brick_size']['field'] = 'field_brick_size';
  $handler->display->display_options['fields']['field_brick_size']['label'] = '';
  $handler->display->display_options['fields']['field_brick_size']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_brick_size']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'brick' => 'brick',
  );

  /* Display: Grid layout */
  $handler = $view->new_display('page', 'Grid layout', 'grid');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Grid layout';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'freewall';
  $handler->display->display_options['style_options']['row_class'] = 'cell';
  $handler->display->display_options['style_options']['row_style'] = 'width:200px; height: 200px; background-image: url([uri])';
  $handler->display->display_options['style_options']['delay'] = '0';
  $handler->display->display_options['style_options']['cellW'] = '200';
  $handler->display->display_options['style_options']['cellH'] = '200';
  $handler->display->display_options['style_options']['gutterX'] = '';
  $handler->display->display_options['style_options']['gutterY'] = '';
  $handler->display->display_options['style_options']['fixSize'] = '';
  $handler->display->display_options['style_options']['draggable'] = 0;
  $handler->display->display_options['style_options']['animate'] = 1;
  $handler->display->display_options['style_options']['rightToLeft'] = 0;
  $handler->display->display_options['style_options']['bottomToTop'] = 0;
  $handler->display->display_options['style_options']['onGapFound'] = '';
  $handler->display->display_options['style_options']['onComplete'] = '';
  $handler->display->display_options['style_options']['onResize'] = 'this.refresh();
';
  $handler->display->display_options['style_options']['onBlockReady'] = '';
  $handler->display->display_options['style_options']['onBlockActive'] = '';
  $handler->display->display_options['style_options']['onBlockFinish'] = '';
  $handler->display->display_options['style_options']['before'] = '';
  $handler->display->display_options['style_options']['after'] = 'wall.fitWidth();
$(window).trigger("resize");

';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: File: Path */
  $handler->display->display_options['fields']['uri']['id'] = 'uri';
  $handler->display->display_options['fields']['uri']['table'] = 'file_managed';
  $handler->display->display_options['fields']['uri']['field'] = 'uri';
  $handler->display->display_options['fields']['uri']['relationship'] = 'field_brick_image_fid';
  $handler->display->display_options['fields']['uri']['label'] = '';
  $handler->display->display_options['fields']['uri']['exclude'] = TRUE;
  $handler->display->display_options['fields']['uri']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['uri']['file_download_path'] = TRUE;
  $handler->display->display_options['path'] = 'freewall/grid';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'Grid layout';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'Freewall';
  $handler->display->display_options['tab_options']['weight'] = '1';
  $handler->display->display_options['tab_options']['name'] = 'main-menu';

  /* Display: Pinterest like layout  */
  $handler = $view->new_display('page', 'Pinterest like layout ', 'pinterest');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Pinterest layout ';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'freewall';
  $handler->display->display_options['style_options']['row_class'] = 'brick';
  $handler->display->display_options['style_options']['row_style'] = '';
  $handler->display->display_options['style_options']['delay'] = '0';
  $handler->display->display_options['style_options']['cellW'] = '200';
  $handler->display->display_options['style_options']['cellH'] = 'auto';
  $handler->display->display_options['style_options']['gutterX'] = '';
  $handler->display->display_options['style_options']['gutterY'] = '';
  $handler->display->display_options['style_options']['fixSize'] = '';
  $handler->display->display_options['style_options']['draggable'] = 0;
  $handler->display->display_options['style_options']['animate'] = 1;
  $handler->display->display_options['style_options']['rightToLeft'] = 0;
  $handler->display->display_options['style_options']['bottomToTop'] = 0;
  $handler->display->display_options['style_options']['onGapFound'] = '';
  $handler->display->display_options['style_options']['onComplete'] = '';
  $handler->display->display_options['style_options']['onResize'] = 'this.fitWidth();';
  $handler->display->display_options['style_options']['onBlockReady'] = '';
  $handler->display->display_options['style_options']['onBlockActive'] = 'console.log(\'onBlockActive\');';
  $handler->display->display_options['style_options']['onBlockFinish'] = '';
  $handler->display->display_options['style_options']['before'] = '';
  $handler->display->display_options['style_options']['after'] = 'var images = wall.container.find(\'.brick\');
var length = images.length;
images.css({visibility: \'hidden\'});
images.find(\'img\').load(function() {
  -- length;
  if (!length) {
    setTimeout(function() {
      images.css({visibility: \'visible\'});
      wall.fitWidth();
    }, 505);
  }
});
';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: File: Path */
  $handler->display->display_options['fields']['uri']['id'] = 'uri';
  $handler->display->display_options['fields']['uri']['table'] = 'file_managed';
  $handler->display->display_options['fields']['uri']['field'] = 'uri';
  $handler->display->display_options['fields']['uri']['relationship'] = 'field_brick_image_fid';
  $handler->display->display_options['fields']['uri']['label'] = '';
  $handler->display->display_options['fields']['uri']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['uri']['alter']['text'] = '<img src="[uri]" width="100%">';
  $handler->display->display_options['fields']['uri']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['uri']['file_download_path'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<h3>Freewall</h3>
<h5>[title]</h5>';
  $handler->display->display_options['fields']['title']['element_type'] = 'div';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['path'] = 'freewall/pinterest';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Pinterest layout ';
  $handler->display->display_options['menu']['weight'] = '1';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Flex grid layout */
  $handler = $view->new_display('page', 'Flex grid layout', 'flex_grid');
  $handler->display->display_options['path'] = 'freewall/flex-grid';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Flex grid';
  $handler->display->display_options['menu']['weight'] = '2';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['freewall'] = $view;

  return $export;
}
