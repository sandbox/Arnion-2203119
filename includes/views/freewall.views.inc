<?php
/**
 * @file
 * Views integration for Freewall module.
 */

/**
 * Implements hook_views_plugin().
 */
function freewall_views_plugins() {
  $path = drupal_get_path('module', 'freewall') . '/includes/views/';
  $plugins['style']['freewall'] = array(
    'title' => t('Freewall'),
    'help' => t('Display rows in a responsive grid layouts via Freewall.'),
    'handler' => 'freewall_style_plugin',
    'path' => $path . 'plugins',
    'theme' => 'freewall_view',
    'theme path' => $path . 'theme',
    'theme file' => 'freewall.views.theme.inc',
    'uses row plugin' => TRUE,
    'uses options' => TRUE,
    'uses grouping' => FALSE,
    'type' => 'normal',
  );
  return $plugins;
}
