<?php

/**
 * @file
 * Contains the Freewall style plugin.
 */

/**
 * Style plugin to render each item.
 *
 * @ingroup views_style_plugins
 */
class freewall_style_plugin extends views_plugin_style {

  /**
   * The options definition.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['row_class'] = array('default' => 'brick');
    $options['row_style'] = array('default' => NULL);
    $options['delay'] = array('default' => 0);
    $options['cellW'] = array('default' => NULL);
    $options['cellH'] = array('default' => NULL);
    $options['gutterX'] = array('default' => NULL);
    $options['gutterY'] = array('default' => NULL);
    $options['fixSize'] = array('default' => NULL);
    $options['draggable'] = array('default' => FALSE);
    $options['animate'] = array('default' => FALSE);
    $options['rightToLeft'] = array('default' => FALSE);
    $options['bottomToTop'] = array('default' => FALSE);

    $options['onGapFound'] = array('default' => FALSE);
    $options['onComplete'] = array('default' => FALSE);
    $options['onResize'] = array('default' => FALSE);
    $options['onBlockReady'] = array('default' => FALSE);
    $options['onBlockActive'] = array('default' => FALSE);
    $options['onBlockFinish'] = array('default' => FALSE);

    $options['before'] = array('default' => FALSE);
    $options['after'] = array('default' => FALSE);

    return $options;
  }

  /**
   * The configuration form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $field_options = array();
    $fields = $this->display->handler->get_handlers('field');
    foreach ($fields as $id => $handler) {
      $field_options[$id] = $handler->ui_name(FALSE);
    }

    // Class option.
    $form['row_class'] = array(
      '#type' => 'textfield',
      '#title' => t('Class'),
      '#default_value' => $this->options['row_class'],
      '#description' => t('Class attribute of row.'),
    );

    // Style option.
    $form['row_style'] = array(
      '#type' => 'textfield',
      '#title' => t('Styles'),
      '#default_value' => $this->options['row_style'],
      '#description' => t('Style attribute of row.'),
    );

    // Get a list of the available fields and arguments for token replacement.
    $options = array();
    foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      $options[t('Fields')]["[$field]"] = $handler->ui_name();
      // We only use fields up to (and including) this one.
      if ($field == $this->options['id']) {
        break;
      }
    }
    // This lets us prepare the key as we want it printed.
    $count = 0;
    foreach ($this->view->display_handler->get_handlers('argument') as $arg => $handler) {
      $options[t('Arguments')]['%' . ++$count] = t('@argument title', array('@argument' => $handler->ui_name()));
      $options[t('Arguments')]['!' . $count] = t('@argument input', array('@argument' => $handler->ui_name()));
    }

    $output = t("<p>The following tokens are available for this field. Note that due to rendering order, you cannot use fields that come after this field; if you need a field not listed here, rearrange your fields.
If you would like to have the characters '[' and ']' please use the html entity codes '%5B' or  '%5D' or they will get replaced with empty space.</p>");
    $items = array(
      '[number] == ' . t('Row number.'),
      '[zebra] == ' . t('Even or odd.'),
      '[is_first] == ' . t('Add the "first" class if the first row'),
      '[is_last] == ' . t('Add the "last" class if the last row'),
    );

    if (!empty($options)) {
      foreach (array_keys($options) as $type) {
        if (!empty($options[$type])) {
          foreach ($options[$type] as $key => $value) {
            $items[] = $key . ' == ' . check_plain($value);
          }
        }
      }
    }

    $output .= theme('item_list',
    array(
      'items' => $items,
      'type' => 'ul',
    ));

    $form['help'] = array(
      '#type' => 'fieldset',
      '#title' => t('Replacement patterns'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#value' => $output,
    );

    // Selector option.
    $form['selector'] = array(
      '#type' => 'textfield',
      '#title' => t('Selector'),
      '#default_value' => $this->options['selector'],
      '#description' => t('Get all blocks to made layout.'),
    );

    // Delay option.
    $form['delay'] = array(
      '#type' => 'textfield',
      '#title' => t('Delay'),
      '#size' => 4,
      '#maxlength' => 4,
      '#default_value' => $this->options['delay'],
      '#field_suffix' => ' ' . t('seconds'),
      '#description' => t('The time delay for show block. It will increase by each block.'),
    );

    // Cell width option.
    $form['cellW'] = array(
      '#type' => 'textarea',
      '#title' => t('Cell width'),
      '#default_value' => $this->options['cellW'],
      '#description' => t('The width of unit, base on it will build grid container.	It can be a function and return value.'),
    );

    // Cell height option.
    $form['cellH'] = array(
      '#type' => 'textarea',
      '#title' => t('Cell height'),
      '#default_value' => $this->options['cellH'],
      '#description' => t('The height of unit, base on it will build grid container. It can be a function and return value.'),
    );

    // Gutter X option.
    $form['gutterX'] = array(
      '#type' => 'textfield',
      '#title' => t('Gutter X'),
      '#default_value' => $this->options['gutterX'],
      '#description' => t("The horizon spacing between the column. Default is number, but can set it with 'auto' value."),
    );

    // Gutter Y option.
    $form['gutterY'] = array(
      '#type' => 'textfield',
      '#title' => t('Gutter Y'),
      '#default_value' => $this->options['gutterY'],
      '#description' => t("The vertical spacing between the row. Default is number, but can set it with 'auto' value."),
    );

    // Fix Size option.
    $form['fixSize'] = array(
      '#type' => 'select',
      '#title' => t('Fix size'),
      '#description' => t('Can override the fixSize option by set data-fixSize attribute in the block.'),
      '#options' => array(
        '' => t('None'),
        'true' => t('Yes'),
        'false' => t('No'),
      ),
      '#default_value' => $this->options['fixSize'],
    );

    // Draggable option.
    $form['draggable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Draggable'),
      '#default_value' => $this->options['draggable'],
      '#description' => t('Make block can be drag & drop.'),
    );

    // Animate option.
    $form['animate'] = array(
      '#type' => 'checkbox',
      '#title' => t('Animate'),
      '#default_value' => $this->options['animate'],
      '#description' => t('Make block move with animation.'),
    );

    // Right to left option.
    $form['rightToLeft'] = array(
      '#type' => 'checkbox',
      '#title' => t('Right to left'),
      '#default_value' => $this->options['rightToLeft'],
      '#description' => t('Let layout start render from right to left.'),
    );

    // Bottom to top option.
    $form['bottomToTop'] = array(
      '#type' => 'checkbox',
      '#title' => t('Bottom to top'),
      '#default_value' => $this->options['bottomToTop'],
      '#description' => t('Let layout start render from bottom up to top.'),
    );

    // Event options.
    $form['events'] = array(
      '#type' => 'fieldset',
      '#title' => t('Events'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#parents' => array('style_options'),
    );

    // onGapFound event.
    $form['events']['onGapFound'] = array(
      '#type' => 'textarea',
      '#title' => t('onGapFound event'),
      '#default_value' => $this->options['onGapFound'],
      '#description' => t('Registry callback when a gap found.'),
    );

    // onComplete event.
    $form['events']['onComplete'] = array(
      '#type' => 'textarea',
      '#title' => t('onComplete event'),
      '#default_value' => $this->options['onComplete'],
      '#description' => t('Registry callback when all block arrange.'),
    );

    // onResize event.
    $form['events']['onResize'] = array(
      '#type' => 'textarea',
      '#title' => t('onResize event'),
      '#default_value' => $this->options['onResize'],
      '#description' => t('Registry callback when browser resize.'),
    );

    // onBlockReady event.
    $form['events']['onBlockReady'] = array(
      '#type' => 'textarea',
      '#title' => t('onBlockReady event'),
      '#default_value' => $this->options['onBlockReady'],
      '#description' => t('Fire when block adjusted.'),
    );

    // onBlockActive event.
    $form['events']['onBlockActive'] = array(
      '#type' => 'textarea',
      '#title' => t('onBlockActive event'),
      '#default_value' => $this->options['onBlockActive'],
      '#description' => t('Fire before block show or hide in the layout.'),
    );

    // onBlockFinish event.
    $form['events']['onBlockFinish'] = array(
      '#type' => 'textarea',
      '#title' => t('onBlockFinish event'),
      '#default_value' => $this->options['onBlockFinish'],
      '#description' => t('Fire when block finish show or hide in the layout.'),
    );

    // Advanced options.
    $form['advanced'] = array(
      '#type' => 'fieldset',
      '#title' => t('Advanced'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#parents' => array('style_options'),
    );

    // Code before.
    $form['advanced']['before'] = array(
      '#type' => 'textarea',
      '#title' => t('Code: before'),
      '#default_value' => $this->options['before'],
      '#description' => t('This code executed before Freewall is initialize.'),
    );

    // Code after.
    $form['advanced']['after'] = array(
      '#type' => 'textarea',
      '#title' => t('Code: after'),
      '#default_value' => $this->options['after'],
      '#description' => t('This code executed after Freewall is initialize.'),
    );

  }
}
