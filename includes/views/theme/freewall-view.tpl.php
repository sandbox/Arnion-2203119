<?php

/**
 * @file freewall-view.tpl.php
 * View template to display a list as a responsive grid layouts.
 */
?>
<div id="<?php print $identifier; ?>">
<?php foreach ($rows as $id => $row): ?>
  <div <?php print $row_attributes[$id]; ?>><?php print $row; ?></div>
<?php endforeach; ?>
</div>