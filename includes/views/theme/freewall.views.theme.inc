<?php

/**
 * @file
 * The theme system, which controls the output of Freewall views .
 */

/**
 * Preprocess function for freewall-view.tpl.php.
 */
function template_preprocess_freewall_view(&$variables) {
  $view = $variables['view'];
  $options = $view->style_options;
  $display_id = empty($view->current_display) ? 'default' : $view->current_display;
  $row_class = isset($options['row_class']) ? $options['row_class'] : '';
  $row_style = isset($options['row_style']) ? $options['row_style'] : '';

  $identifier = drupal_clean_css_identifier('freewall-view--' . $view->name . '--' . $display_id);
  $variables['identifier'] = $identifier;

  // Add necessary JavaScript and CSS.
  freewall_add('#' . $identifier, $options);

  // Views 2/3 compatibility.
  $pager_offset = isset($view->pager['offset']) ? $view->pager['offset'] : $view->offset;

  $field_handler = end($view->field);
  $total = count($variables['rows']);

  // Give each item a class to identify where in the carousel it belongs.
  foreach ($variables['rows'] as $id => $row) {
    $tokens = array();
    $tokens['[number]'] = $number = $id + 1 + $pager_offset;
    $tokens['[zebra]'] = ($number % 2 == 0) ? 'even' : 'odd';
    $tokens['[is_first]'] = ($id == 0) ? 'first' : '';
    $tokens['[is_last]'] = ($id == $total - 1) ? 'last' : '';

    $class = _freewall_render_tokens($row_class, $tokens);
    $style = _freewall_render_tokens($row_style, $tokens);

    $attributes = array();

    if ($field_handler) {
      if (!empty($class)) {
        $attributes['class'] = $field_handler->tokenize_value($class, $id);
      }
      if (!empty($style)) {
        $attributes['style'] = $field_handler->tokenize_value($style, $id);
      }

    }

    $variables['row_attributes'][$id] = drupal_attributes($attributes);
  }

}

/**
 * Replace tokens for values.
 */
function _freewall_render_tokens($input, $tokens) {
  foreach ($tokens as $token => $value) {
    $input = str_replace($token, $value, $input);
  }
  $input = str_replace('   ', ' ', $input);
  $input = str_replace('  ', ' ', $input);
  $input = trim($input);

  return $input;
}
