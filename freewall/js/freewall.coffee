###*
 * @file
 * Freewall behaviors
###

$ = @jQuery
@Drupal.behaviors.freewall =
  attach: (context, settings) ->
    $.each(settings.freewalls, (selector , config) ->
      container = $ selector + ':not(.freewall-processed)'
        .addClass('freewall-processed')
      wallEvents = [
        'onGapFound',
        'onComplete',
        'onResize',
        'onBlockReady',
        'onBlockActive ',
        'onBlockFinish ',
      ]

      $.each(wallEvents, (index , event) ->
        if typeof config[event] is 'string'
          eval("config[event] = function() {" + config[event] + "}")
        else if event is 'onResize'
          config[event] = () ->
            this.fitWidth();
      )

      if typeof config.before is 'string'
        eval(config.before)

      wall = new freewall container
      wall.reset(config)

      if typeof config.after is 'string'
        eval(config.after)
      else
        wall.fitWidth()

      null
    )