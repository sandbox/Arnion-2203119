/**
 * @file
 * Add Freewall behaviors to the page and provide Views-support.
 */

(function() {
  var $;

  $ = this.jQuery;

  this.Drupal.behaviors.freewall = {
    attach: function(context, settings) {
      return $.each(settings.freewall, function(selector, config) {
        var container, wall, wallEvents;
        container = $('#container' + ':not(.freewall-processed)').addClass('freewall-processed'+ ' ' + config.addCssClasses);
        wallEvents = ['onGapFound', 'onComplete', 'onResize', 'onBlockReady', 'onBlockActive ', 'onBlockFinish '];
        $.each(wallEvents, function(index, event) {
          if (typeof config[event] === 'string') {
            return eval("config[event] = function() {" + config[event] + "}");
          } else if (event === 'onResize') {
            return config[event] = function() {
              return this.fitWidth();
            };
          }
        });
        if (typeof config.before === 'string') {
          eval(config.before);
        }
        wall = new freewall(container);
        wall.reset(config);
        if (typeof config.after === 'string') {
          eval(config.after);
        } else {
          wall.fitWidth();
        }
        return null;
      });
    }
  };

}).call(this);
