<?php
namespace Drupal\freewall;
use Drupal\Core\Extension\ModuleHandlerInterface;

class FreewallService  {

    protected $moduleHandler;

	
 /**
   * Constructs a FreewallService object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler
   *
   */
  function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
	//$cssClasses = array(0 => 'none', 1 => 'freewall-pinterest');
  }
	 /**
   * Check if the Freewall librarie is installed.
   *
   * @return
   *   A boolean indicating the installed status.
   */
  function isFreewallInstalled() {
    $freewall = libraries_detect('freewall');
	//kint ($freewall);
    if ((!empty($freewall['installed']))) {
      return TRUE;
    }
    else {
      return TRUE;
    }
  }

 /*   'rightToLeft',
    'bottomToTop',

    'onGapFound',
    'onComplete',
    'onResize',
    'onBlockReady',
    'onBlockActive ',
    'onBlockFinish ',

    'before',
    'after',*/
  
   public function getFreewallDefaultOptions() {
    return array(
      'row_class' => 'brick',
      'row_style' => '',
      'row_selector' => '',
      'delay' => '0',
      'cellW' => '200',
	  'cellH' => 'auto',
      'gutterX' => '',
	  'gutterY' => '',
	  'fixSize' => 'none',
      'draggable' => FALSE,
      'animate' => FALSE,
	  'onResize' => '',
	  'before' => '',
	  'after' => '',
	  'addCssClasses' => 0,
    /*  'rightToLeft' => FALSE,
      'bottomToTop' => FALSE,*/
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildSettingsForm($default_values = array()) {
	$cssClasses = array(0 => 'none', 1 => 'freewall-pinterest');
    // Load module default values if empty.
    if (empty($default_values)) {
      $default_values = $this->getFreewallDefaultOptions();
    }

    $form['row_class'] = array(
      '#type' => 'textfield',
      '#title' => t('Row class'),
      '#description' => t("The class of each element"),
      '#default_value' => $default_values['row_class'],
    );
	$form['row_style'] = array(
      '#type' => 'textfield',
      '#title' => t('Row style'),
      '#description' => t("The styles of each element"),
      '#default_value' => $default_values['row_style'],
    );
	$form['row_selector'] = array(
      '#type' => 'textfield',
      '#title' => t('Selector'),
      '#description' => t("The selector of each element"),
      '#default_value' => $default_values['row_selector'],
    );
	$form['delay'] = array(
      '#type' => 'textfield',
      '#title' => t('Delay'),
      '#description' => t("The delay of each element"),
      '#default_value' => $default_values['delay'],
    );
	$form['cellW'] = array(
      '#type' => 'textfield',
      '#title' => t('Cellwidth'),
      '#description' => t("The cellwidth of each element"),
      '#default_value' => $default_values['cellW'],
    );
	$form['cellH'] = array(
      '#type' => 'textfield',
      '#title' => t('Cellheight'),
      '#description' => t("The cellheight of each element"),
      '#default_value' => $default_values['cellH'],
    );
	$form['gutterX'] = array(
      '#type' => 'textfield',
      '#title' => t('Gutter X'),
      '#description' => t("The gutter X of each element"),
      '#default_value' => $default_values['gutterX'],
    );
	$form['gutterY'] = array(
      '#type' => 'textfield',
      '#title' => t('Gutter Y'),
      '#description' => t("The gutter Y of each element"),
      '#default_value' => $default_values['gutterY'],
    );
	$form['fixSize'] = array(
       '#type' => 'select',
       '#title' => t('Fix size'),
       '#options' => ['none','TRUE','FALSE'], //I'm getting this list from my database
       '#multiple' => FALSE, 
	   '#default_value' => $default_values['fixSize'],	   
   );
	$form['draggable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Draggable'),
      '#description' => t("Items are draggable."),
      '#default_value' => $default_values['draggable'],
    );
	$form['animate'] = array(
      '#type' => 'checkbox',
      '#title' => t('Animate'),
      '#description' => t("Items are animated."),
      '#default_value' => $default_values['animate'],
    );
	$form['onResize'] = array(
      '#title' => t('onResize event'),
      '#type' => 'textarea',
      '#description' => 'Registry callback when browser is resized.',
      '#default_value' => $default_values['onResize'],
      '#rows' => 10,
      '#cols' => 60,
      '#resizable' => TRUE,
    );
	$form['before'] = array(
      '#title' => t('Code:before'),
      '#type' => 'textarea',
      '#description' => 'This code is executed before Freewall is initialized',
      '#default_value' => $default_values['before'],
      '#rows' => 10,
      '#cols' => 60,
      '#resizable' => TRUE,
    );
	$form['after'] = array(
      '#title' => t('Code:after'),
      '#type' => 'textarea',
      '#description' => 'This code is executed after Freewall is initialized',
      '#default_value' => $default_values['after'],
      '#rows' => 10,
      '#cols' => 60,
      '#resizable' => TRUE,
    );
	$form['addCssClasses'] = array(
       '#type' => 'select',
       '#title' => t('Add css classes'),
       '#options' => $cssClasses,
       '#multiple' => FALSE, 
	   '#default_value' => $default_values['addCssClasses'],	   
   );
  

    // Allow other modules to alter the form.
    $this->moduleHandler->alter('freewall_options_form', $form, $default_values);

    return $form;
 }

  /**
   * Apply Masonry to a container.
   *
   * @param $container
   *   The CSS selector of the container element to apply Masonry to.
   * @param $options
   *   An associative array of Masonry options.
   *   Contains:
   *   - masonry_item_selector: The CSS selector of the items within the
   *     container.
   *   - masonry_column_width: The width of each column (in pixels or as a
   *     percentage).
   *   - masonry_column_width_units: The units to use for the column width ('px'
   *     or '%').
   *   - masonry_gutter_width: The spacing between each column (in pixels).
   *   - masonry_resizable: Automatically rearrange items when the container is
   *     resized.
   *   - masonry_animated: Animate item rearrangements.
   *   - masonry_animation_duration: The duration of animations (in milliseconds).
   *   - masonry_fit_width: Sets the width of the container to the nearest column.
   *     Ideal for centering Masonry layouts.
   *   - masonry_rtl: Display items from right-to-left.
   *   - masonry_images_first: Load all images first before triggering Masonry.
   */
  public function applyFreewallDisplay(&$form, $container, $item_selector, $options = array()) {
	$cssClasses = array(0 => 'none', 1 => 'freewall-pinterest');
   // if (masonry_loaded() && !empty($container)) {
    if (!empty($container)) {
      // For any options not specified, use default options
      $options += $this->getFreewallDefaultOptions();
      if (!isset($item_selector)) {
        $item_selector = '';
      }

      // Setup Masonry script
      $freewall = array(
        'freewall' => array(
          $container => array(
		    'item_selector' => $item_selector,
			'selector' => '#container',
            'row_class' => $options['row_class'],
            'row_style' => $options['row_style'],
            'selector' => $options['row_selector'],
			'delay' => $options['delay'],
            'cellW' => $options['cellW'],
            'cellH' => $options['cellH'],
			'gutterX' => $options['gutterX'],
            'gutterY' => $options['gutterY'],
            'fixSize' => $options['fixSize'],
			'draggable' => $options['draggable'],
			'animate' => $options['animate'],
			'onResize' => $options['onResize'],
			'before' => $options['before'],
			'after' => $options['after'],
			'addCssClasses' => $cssClasses[$options['addCssClasses']],
          ),
        ),
      );
	  
      // Allow other modules to alter the Masonry script
      $context = array(
        'container' => $container,
        'item_selector' => $item_selector,
        'options' => $options,
      );
      $this->moduleHandler->alter('freewall_script', $freewall, $context);

      $form['#attached']['library'][] =  'freewall/freewall.layout';
      $form['#attached']['drupalSettings'] = $freewall;
    }
  }

  


}