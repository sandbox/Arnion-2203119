<?php
/**
 * @file
 * Contains \Drupal\hello_world\Controller\HelloController.
 */
namespace Drupal\freewall\Controller;

use Drupal\freewall\FreewallService;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**use Drupal\Core\Controller\ControllerBase;*/
class FreewallController extends ControllerBase {
	
	 protected $freewallService;
	 
	 /**
   * Class constructor.
   */

public function __construct() {
  //$this->freewallService = $freewall;
   $freewallService=\Drupal::service('freewall.service');
   
}

//public static function create(ContainerInterface $container) {
 // return new static(
  //  $freewallService=$container->get('freewall');
 // $container->get('freewall');
 // $freewallService=\Drupal::service('freewall');
  
//  );
//}
 /**
   * {@inheritdoc}
   */

 
	/* public function freewall()

    {
        $freewallService = new FreewallService();
		
		 $text= ($freewallService->isFreewallInstalled()) ? 'true' : 'false';
		 
		  return array(
      '#type' => 'markup',
      '#markup' => t($text),  
    );
    }*/
	/**
   * Example info page.
   *
   * @return array
   *   A renderable array.
   */

	
  public function info() {
    $build['content'] = [
      'first_line' => [
        '#prefix' => '<p>',
        '#markup' => 'Drupal includes jQuery.',
        '#suffix' => '</p>',
      ],
      'second_line' => [
        '#prefix' => '<p>',
        '#markup' => 'The freewall:',
        '#suffix' => '</p>',
      ],
      'examples_list' => [
        '#theme' => 'item_list',
        '#items' => [
          'A freewall effect. This demonstrates calling a Freewall library function.',
        ],
        '#type' => 'ol',
      ],
    ];

    return $build;
  }
  public function content() {
	 $freewallService=\Drupal::service('freewall.service');
	 $freewall_installed= $freewallService->isFreewallInstalled();
	 $freewall_installed_text=($freewall_installed) ? 'true' : 'false';
  //   $freewall = isFreewallInstalled();
//	 kint($freewallService);
	/*   if (!empty($freewall['installed']) ) {
      return TRUE;
    }
    else {
      return FALSE;
    }*/
  
	 
	 // $freewall_installed = isFreewallInstalled();
	/**  $text=($freewall_installed) ? 'true' : 'false';*/
	   
	
    return array(
      '#type' => 'markup',
      '#markup' => t( 'freewall installed:'.$freewall_installed_text),  
    );
  }



}
  
