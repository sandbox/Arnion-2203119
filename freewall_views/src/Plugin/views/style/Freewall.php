<?php

/**
 * @file
 * Contains \Drupal\views\Plugin\views\style\Freewall.
 *
 */

namespace Drupal\freewall_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Style plugin to render each item in a Freewall Layout.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "freewall",
 *   title = @Translation("Freewall"),
 *   help = @Translation("Display the results in a Freewall layout."),
 *   theme = "views_view_freewall",
 *   display_types = {"normal"}
 * )
 */
class Freewall extends StylePluginBase {

  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Does the style plugin support grouping of rows.
   *
   * @var bool
   */
  protected $usesGrouping = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    // Get default options from Freewall.
    $default_options = \Drupal::service('freewall.service')->getFreewallDefaultOptions();

    // Set default values for Freewall
    foreach ($default_options as $option => $default_value) {
      $options[$option] = array(
        'default' => $default_value,
      );
      if (is_int($default_value)) {
        $options[$option]['bool'] = TRUE;
      }
    }
//kint($options);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
	  \Drupal::logger('freewall_views')->notice('ok');
    parent::buildOptionsForm($form, $form_state);

    // Add Freewall options to views form
    $form['freewall'] = array(
      '#type' => 'details',
      '#title' => t('Freewall'),
      '#open' => TRUE,
    );
    if (\Drupal::service('freewall.service')->isFreewallInstalled()) {
      $form += \Drupal::service('freewall.service')->buildSettingsForm($this->options);
	  	  \Drupal::logger('freewall_views')->notice('service');
		 // console.log($form);
		//  kint($form);
		//  \Drupal::logger('freewall_views')->notice($form);

      // Display each option within the Freewall fieldset
     foreach (\Drupal::service('freewall.service')->getFreewallDefaultOptions() as $option => $default_value) {
        $form[$option]['#fieldset'] = 'freewall';
      }


      // Views doesn't use FAPI states, so set dependencies instead
   /*   $form['freewall_animated']['#dependency'] = array(
        'edit-style-options-freewall-resizable' => array(1),
      );*/
 /*     $form['freewall_animation_duration']['#dependency'] = array(
        'edit-style-options-freewall-animated' => array(1),
      );*/
    }
		  return $form;
   // else {
      // Disable freewall as plugin is not installed
  /*    $form['freewall_disabled'] = array(
        '#markup' => t('These options have been disabled as the jQuery Freewall plugin is not installed.'),
        '#fieldset' => 'freewall',
      );*/
   // }
  }
}
